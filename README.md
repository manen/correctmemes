# CorrectMemes

CorrectMemes is software that automates the process of posting memes on Instagram. Made for the Instagram meme account "correctmemes".

## Concept

It looks at Reddit, and then it decides what memes to post. It asks for permission from the selected memes' owners, and when that's given, it posts the meme, and a screenshot of the conversation between the bot and the meme owner.

## Disclaimer

The software is being developed, not currently ready for use.
