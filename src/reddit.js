const reddit = require("./redditapi");

const fs = require("fs").promises;

var selectedPosts = [];

async function permAsk(postId) {
    if (!postId.startsWith("t3_")) postId = "t3_" + postId;

    var replyId;

    if (selectedPosts[postId])
        throw new Error(
            "Permission for " + postId + " has already been asked."
        );

    try {
        replyId = await reddit
            .getSubmission(postId)
            .reply(
                'Hi! Can we post this meme on our meme page? You and the subreddit will be credited. For automation reasons, any reply containing "yes" will be considered proper permission.'
            );
    } catch (e) {
        console.log(e);
    }

    selectedPosts[postId] = {
        replyId,
        perm: false,
        permReplyId: null
    };
}

async function permCheck(postId) {
    if (!postId.startsWith("t3_")) postId = "t3_" + postId;

    if (!selectedPosts[postId])
        throw new Error("Permission for " + postId + " hasn't been asked.");

    try {
        const post = await reddit.getSubmission(postId);

        await (
            await (
                await reddit.getSubmission(selectedPosts[postId].replyId)
            ).expandReplies({ limit: Infinity, depth: Infinity })
        ).thread.forEach(reply => {
            if (reply.author != post.author) return;
            else {
                reply = await(reply.fetch());
                selectedPosts[postId].permReplyId = reply.id;

                if (reply.content.includes("yes"))
                    selectedPosts[postId].perm = true;
            }
        });
    } catch (e) {
        console.log(e);
    }
}

async function permAsks() {
    Object.entries(selectedPosts).forEach(a => {
        if (!a[1]) permAsk(a[0]);
    });
}

async function permChecks() {
    Object.entries(selectedPosts).forEach(a => {
        if (a[1] && !a[1].perm) permCheck(a[0]);
    });
}

async function collect() {
    return selectedPosts.filter(a => {
        if (a && a.perm) return true;
        else return false;
    });
}

async function cleanup() {
    selectedPosts = selectedPosts.filter(a => {
        if (a && a.perm) return false;
        else return true;
    });
}

// Saves sync, I won't even try running a promise on a process event c:
function save() {
    const fs = require("fs");

    fs.writeFileSync("./redditdata.json", JSON.stringify({ selectedPosts }));
}

async function load() {
    selectedPosts = JSON.parse(await fs.readFile("./redditdata.json"))
        .selectedPosts;
}

load();

[
    `exit`,
    `SIGINT`,
    `SIGUSR1`,
    `SIGUSR2`,
    `uncaughtException`,
    `SIGTERM`
].forEach(eventType => {
    process.on(eventType, save.bind(null, eventType));
});

module.exports = {
    permAsk,
    permCheck,
    permAsks,
    permChecks,
    collect,
    cleanup
};
